<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardDefunctTestimonialsManageController extends Controller {
	
	/*
	 * Passes variables to the single_page/dashboard/testimonials/manage view
	 */
	public function view() {
		
		if($this->getList()) {
			$this->set('testimonials', $this->getList());
		}
		
		// Edit testimonials
		if($_GET['task'] == 'edit') {
			$r = $this->getTestimonial($_GET['pID']);
			$this->set('author', $r['author']);
			$this->set('testimonial', $r['testimonial']);
			$this->set('optionalContent', $r['optionalContent']);
		}
	}
	
	/*
	 * Updates a testimonial
	 */	
	public function update() {
		if($_POST['edit_testimonial']) {
			$db = Loader::db();
			$v = array($_POST['author'], $_POST['testimonial'], $_POST['optionalContent'], intval($_POST['pID']));
			$r = $db->prepare("UPDATE pkTestimonials SET author=?, testimonial=?, optionalContent=? where pID = ?");
			$res = $db->execute($r, $v);
			$this->redirect('/dashboard/defunct_testimonials/manage/?message=3');
		}
	}
	
	/*
	 * Deletes a testimonial from the database and forwards back
	 */
	public function delete() {
		if($_GET['pID']) {
			$db = Loader::db();
			$r = $db->query("DELETE FROM pkTestimonials WHERE pID = ?",array(intval($_GET['pID'])) );
			$this->redirect('/dashboard/defunct_testimonials/manage/?message=1');
		} else {
			$this->redirect('/dashboard/defunct_testimonials/manage/?error=1');
		}
	
	}	
	
	/*
	 * Provides a list of testimonials from the database
	 */
	private function getList() {
		$db = Loader::db();
		$q = "SELECT pID, author, testimonial, optionalContent FROM pkTestimonials ORDER BY sort";
		$r = $db->query($q);
		
		if ($r && $r->numRows() > 0) {
			
			while ($row = $r->fetchRow()) {
				$rows[] = $row;
			}
			
			return $rows;
		} else {
			return FALSE;
		}
	}

	/*
	 * Returns a single testimonial
	 */	
	private function getTestimonial($pID = 0) {
		$db = Loader::db();
		$q = "SELECT pID, author, testimonial, optionalContent FROM pkTestimonials WHERE pID = ?";
		$r = $db->GetRow($q, array(intval($pID)));
		
		return $r;
	}
	
}