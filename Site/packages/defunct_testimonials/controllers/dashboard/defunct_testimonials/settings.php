<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardDefunctTestimonialsSettingsController extends Controller {

	/*
	 * Passes our saved values to the view
	 */	
	public function view() {
		$db = Loader::db();
		
		$display_number= $db->GetOne('SELECT numberToDisplay FROM pkTestimonialsSettings');
		$this->set('display_number', $display_number);		
	}
	
	/*
	 * Saves the testimonial settings
	 */		
	public function update() {
		if($_POST['update_settings']) {
			$db = Loader::db();
			$v = array(intval($_POST['numberToDisplay']));
			$r = $db->prepare("UPDATE pkTestimonialsSettings SET numberToDisplay = ?");
			$res = $db->execute($r, $v);
			$this->redirect('/dashboard/defunct_testimonials/settings/?message=1');
		}
	}
}