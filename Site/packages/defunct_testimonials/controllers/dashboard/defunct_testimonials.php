<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardDefunctTestimonialsController extends Controller {

	public function __construct() {
		$this->redirect('/dashboard/defunct_testimonials/manage');
	}

}