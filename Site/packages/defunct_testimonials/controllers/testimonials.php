<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));


class TestimonialsController extends Controller {	
	
	public function view($page = 0) {
		// include stylesheet in the header
		$html = Loader::helper('html');
		$this->addHeaderitem($html->css(DIR_REL.'/packages/defunct_testimonials/stylesheets/testimonials.css'));
		$db = Loader::db();
		
		// number of testimonials per page
		$display_number	= $db->GetOne("SELECT numberToDisplay FROM pkTestimonialsSettings");
		
		$total = $db->GetOne('SELECT count(pID) FROM pkTestimonials');
		$pageBase = View::url('/testimonials', 'view');
		$paginator = Loader::helper('pagination');
		$paginator->init(intval($page), $total, $pageBase . '/%pageNum%', intval($display_number));
		$limit=$paginator->getLIMIT();								
		
		// grab our testimonials
		$q = "SELECT pID, author, testimonial, optionalContent FROM pkTestimonials ORDER BY sort LIMIT $limit";
		$r = $db->query($q);
		
		if ($r && $r->numRows() > 0) {
			
			while ($row = $r->fetchRow()) {
				$rows[] = $row;
			}
			
			$this->set('testimonials', $rows);
			$this->set('paginator', $paginator);			
		}		
				
	}
	
}