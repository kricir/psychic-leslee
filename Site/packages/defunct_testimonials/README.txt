Testimonial Concrete5 Block
####################################################################################
Created: 07/07/2009
By: Justin Frydman
####################################################################################

This package can be automatically installed via the marketplace. If for some reason
the marketplace installation does not work, please unzip testimonials.zip and upload
the 'testimonials' folder to the packages directory.

Login to the dashboard and navigate to 'Add Functionality' and the testimonials package
will be available for install.

Once the install is complete you will have access to a testimonials dashboard page, which
includes settings, as well as a testimonials single_page and block. Full documentation is
also available within the dashboard under Testimonials > Documentatino once the package 
is installed.