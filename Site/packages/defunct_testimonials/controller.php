<?php    

defined('C5_EXECUTE') or die(_("Access Denied."));

class DefunctTestimonialsPackage extends Package {

	protected $pkgHandle = 'defunct_testimonials';
	protected $appVersionRequired = '5.6';
	protected $pkgVersion = '1.5';
	
	public function getPackageDescription() {
		return t("Manages testimonials and installs the testimonials block");
	}
	
	public function getPackageName() {
		return t("Testimonials");
	}
	
	public function install() {
		$pkg = parent::install();
		
		Loader::model('single_page');
		$db = Loader::db();
		
		// install block		
		$bt = BlockType::installBlockTypeFromPackage('defunct_testimonials', $pkg);
		
		// add dashboard pages
		$t1 = SinglePage::add('/dashboard/defunct_testimonials', $pkg);
		$t1->update(array('cName'=>t('Testimonials'), 'cDescription'=>t('Manage your testimonials')));
		
		$t2 = SinglePage::add('/dashboard/defunct_testimonials/manage', $pkg);
		$t2->update(array('cName'=>t('Manage Testimonials'), 'cDescription'=>t('Manage your testimonials')));		
		
		$t3 = SinglePage::add('/dashboard/defunct_testimonials/change_order', $pkg);
		$t3->update(array('cName'=>t('Change Testimonial Order'), 'cDescription'=>t('Change the order of the testimonials')));		
		
		$t4 = SinglePage::add('/dashboard/defunct_testimonials/settings', $pkg);
		$t4->update(array('cName'=>t('Settings'), 'cDescription'=>t('Manage Single Page Settings')));		
		
		$t5 = SinglePage::add('/dashboard/defunct_testimonials/documentation', $pkg);
		$t5->update(array('cName'=>t('Documentation'), 'cDescription'=>t('Testimonials Package Documentation')));
		
				
		// add frontend pages
		SinglePage::add('/testimonials', $pkg);

		// required because uninstall does not remove all tables		
		$db->query('DELETE FROM pkTestimonialsSettings');
		// insert default settings
		$db->query("INSERT INTO pkTestimonialsSettings (numberToDisplay) VALUES (5)");			
		
	}
}