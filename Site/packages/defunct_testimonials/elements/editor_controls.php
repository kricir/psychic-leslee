<?php     defined('C5_EXECUTE') or die(_("Access Denied.")); ?> 
<div class="ccm-editor-controls">
<div class="ccm-editor-controls-right-cap">
<ul>
<li><a id="image-selector" href="<?php    echo REL_DIR_FILES_TOOLS_REQUIRED?>/files/search_dialog?search=1" onclick="ccm_editorCurrentAuxTool='image'; setBookMark();" class="dialog-launch" dialog-modal="false" ><?php    echo t('Add Image')?></a></li>
<li><a id="file-selector" href="<?php    echo REL_DIR_FILES_TOOLS_REQUIRED?>/files/search_dialog?search=1" onclick="ccm_editorCurrentAuxTool='file'; setBookMark();" class="dialog-launch" dialog-modal="false" ><?php    echo t('Add File')?></a></li>
<li><a id="link-selector" href="<?php    echo REL_DIR_FILES_TOOLS_REQUIRED?>/sitemap_overlay.php?sitemap_mode=select_page" onclick="setBookMark();" class="dialog-launch" dialog-modal="false" ><?php    echo t('Insert Link to Page')?></a></li>
</ul>
</div>
</div>
<div id="rich-text-editor-image-fm-display">
<input type="hidden" name="fType" class="ccm-file-manager-filter" value="<?php    echo FileType::T_IMAGE?>" />
</div>

<div class="ccm-spacer">&nbsp;</div>

<script type="text/javascript">
  $(function() {  
          $("#link-selector").dialog({ title: '<?php    echo t("Insert Link to Page in Sitemap"); ?>' });     
          $("#file-selector").dialog({ width: 900, title: '<?php    echo t("Add File"); ?>' });     
          $("#image-selector").dialog({ width: 900, title: '<?php    echo t("Add Image"); ?>' });     
  });
</script>
