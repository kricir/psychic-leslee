<?php     defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<?php    if (count($t) > 0) { ?>

	<div class="block-testimonial">
		<img src="<?php    echo DIR_REL.'/packages/defunct_testimonials/blocks/defunct_testimonials/images/quotes.png'; ?>" alt="Quotes" />
		<?php    echo $t['testimonial'] ?>
	</div>
    
    <div class="block-testimonial-options">
    	<p><em>&mdash; <?php    echo htmlspecialchars($t['author']) ?></em>
        <?php    if($t['optionalContent']): ?>
        	<br />
        	<?php    echo nl2br(ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]", "<a href=\"\\0\">\\0</a>", htmlspecialchars($t['optionalContent'])))?>
        <?php    endif; ?>
        </p>
    </div>

<?php    } else { ?>
	<p><?php    echo t('There are no testimonials to display')?></p>
<?php    } ?>