<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<?php    if($testimonials) { ?>

	<?php    foreach($testimonials as $t): ?> 
		<div class="testimonial">
			<img src="<?php    echo DIR_REL.'/packages/defunct_testimonials/blocks/defunct_testimonials/images/quotes.png'?>" alt="Quotes" />
			<?php    echo $t['testimonial']; ?>
    		<p><em>&mdash; <?php    echo htmlspecialchars($t['author']) ?></em>
        <?php    if($t['optionalContent']): ?>
        	<br />
        	<?php    echo nl2br(ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]", "<a href=\"\\0\">\\0</a>", htmlspecialchars($t['optionalContent'])))?>
        <?php    endif; ?>
        </p>
		</div>
        <div class="clear"></div>
	<?php    endforeach; ?>

<?php    } else { ?>
	<p><?php    echo t('No testimonials found');?></p>
<?php    } ?>

<?php     if($paginator && strlen($paginator->getPages())>0){ ?>	 
	 <div  class="pagination">
		 <div class="pageLeft"><?php    echo str_replace('view//', 'view/', $paginator->getPrevious() );?></div>
		 <div class="pageRight"><?php    echo str_replace('view//', 'view/', $paginator->getNext() )?></div>
		 <?php    echo $paginator->getPages()?>
	 </div>		
<?php     } ?>