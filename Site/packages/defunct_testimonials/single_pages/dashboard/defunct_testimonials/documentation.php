<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
?>

<div class="ccm-ui">
	<div class="ccm-pane">

		<?php  echo ConcreteDashboardHelper::getDashboardPaneHeader(); ?>

		<div class="ccm-pane-body ccm-pane-body-footer">

				<p>Created: 07/07/2009 under the <a href="<?php    echo DIR_REL.'/packages/defunct_testimonials/license.txt';?>">Single Use, Limited License</a>
				<br />By: Justin Frydman
				<br />Version: <?php    loader::packageElement('version', 'defunct_testimonials')?> - <a href="<?php    echo DIR_REL.'/packages/defunct_testimonials/CHANGELOG'; ?>">Changelog</a>
				</p>
				
				<h2>Table of Contents</h2>

				<ol>
				<li><a href="#section1">Introduction &amp; Usage</a></li>
				<li><a href="#section2">Installation</a></li>
				<li><a href="#section3">Customization</a></li>
				</ol>
				
				<!-- INTRODUCTION -->
				<h2 id="section1">1) Introduction &amp; Usage</h2>
				
				<p>The Testimonials package comes with a few features you should be aware of.</p>
				
				<ol>
				<li><a href="<?php    echo $this->url('/dashboard/defunct_testimonials'); ?>">Integrated concrete5 backend</a>: This section allows you to add/edit/delete testimonials</li>
				<li><a href="<?php    echo $this->url('/testimonials'); ?>">Testimonials single_page</a>: This page displays all the testimonials that
				   you have entered and is configurable under the <a href="<?php    echo $this->url('/dashboard/defunct_testimonials/settings'); ?>">Settings</a> 
				   page in the dashboard. You can choose how many testimonials per page
			       to display and pagination will automatically kick in.</li>
			    <li><a href="<?php    echo $this->url('/dashboard/defunct_testimonials/change_order'); ?>">Testimonials Change Order page</a>: This page allows you to change the order of the testimonials displayed on the frontend single_page.</li>
				<li>Testimonials block: This block can be placed in any area and will load a
				   random testimonial from the database to display. It comes with basic styles
				   and can be customized by a developer (See section 3).</li>
				</ol>
				
				<!-- INSTALLATION -->
				<h2 id="section2">2) Installation</h2>
				
				<p>This package can be automatically installed via the marketplace. If for some reason
			the marketplace installation does not work, please unzip testimonials.zip and upload
			the 'testimonials' folder to the packages directory.</p>

				<p>Login to the dashboard and navigate to 'Add Functionality' and the testimonials package
				will be available for install.</p>
				
				<p>Once the install is complete you will have access to a <a href="<?php    echo $this->url('/dashboard/defunct_testimonials'); ?>">testimonials dashboard page</a>, which
				includes <a href="<?php    echo $this->url('/dashboard/defunct_testimonials/settings'); ?>">settings</a> and the ability to <a href="<?php    echo $this->url('/dashboard/defunct_testimonials/change_order'); ?>">change the order of the testimonials</a>, as well as a <a href="<?php    echo $this->url('/testimonials'); ?>">testimonials single_page</a> and block.</p>
				
				
				<!-- CUSTOMIZATION -->
				<h2 id="section3">3) Customization</h2>
				
				<p>Both the single_page and block can be styled with css.</p>

				<ol>
				<li>single_page: The single_page is currently including a stylesheet from
				the packages/testimonials/stylesheets/ folder. Any developer with CSS
				knowledge can customize the look and feel of the single_page /testimonials
				output.</li>
				<li>block: Should you wish to customize the block appearance is recommend that you
				   copy the block to your personal blocks folder so that it will not be overwritten
				   during upgrades. From there you can customize both view.css and view.php to your
			  	   liking.</li>
				<li>It is also possible to create the folder structure in the blocks directory as follows:
					blocks/testimonials/templates/YOUR_TEMPLATE_NAME.php
				   You may also include a view.css file. After the block has been added to page, simply
				   click on it and select "Set Custom Template" and choose the template you created.</li>
				</ol>
				
				<p>Thank you very much for purchasing this concrete5 package. Any questions, bugs or feature 
			requests should be directed to the <a href="http://www.concrete5.org/marketplace/addons/defunct_testimonials/support_and_feature_requests/">bug tracker located on this packages marketplace page</a>.</p>

			  <p>Justin Frydman<br />
			  <a href="http://defunctlife.com">http://defunctlife.com</a><br />
			  <a href="http://twitter.com/defunctlife">http://twitter.com/defunctlife</a></p>
			  
		</div>
	</div>
</div>