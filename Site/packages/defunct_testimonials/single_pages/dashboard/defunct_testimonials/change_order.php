<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>


<!-- UPDATED VIA AJAX -->
<div class="ccm-ui">
	<div id="status"></div>	
</div>

<div class="ccm-ui">
	<div class="ccm-pane">

		<?php  echo ConcreteDashboardHelper::getDashboardPaneHeader(); ?>

		<div class="ccm-pane-body ccm-pane-body-footer">	

			<h3><?php    echo t('Drag and Drop Your Testimonials to Change Their Order'); ?></h3>
			
			<ul id="testimonial-order">
			<?php    foreach($testimonials as $t): ?> 
				<li id="sort_<?php    echo $t['pID']; ?>"><?php    echo htmlspecialchars($t['author']) ?></li>
			<?php    endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<script type="text/javascript">
// drag and drop change_order.php dashboard js
$(document).ready(function(){ 
						   
	$(function() {
		$("#testimonial-order").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = $(this).sortable("serialize"); 
			$.post("<?php  echo $this->action('ajax'); ?>", order, function(theResponse){		
				$("#status").html(theResponse);				
			}); 															 
		}								  
		});
	});
 
});	
</script>