<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

$ih = Loader::helper('concrete/interface');
?>
<div class="ccm-ui">
	<?php  
		// Display success messages
		if($_GET['message']) {
		
			$display = '';
			
			switch(intval($_GET['message'])) {
				case 1:
					$display = t('Settings Saved');
					break;	
			}
			
			echo '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">×</button>'. $display .'</div>';		
		}
	?>
</div>

<div class="ccm-ui">
	<div class="ccm-pane">

		<?php  echo ConcreteDashboardHelper::getDashboardPaneHeader(); ?>

		<div class="ccm-pane-body ccm-pane-body-footer">


			
			<form method="post" id="update_settings_form" action="<?php    print $this->action('update')?>">
				<fieldset>
					<div class="clearfix">
						<label style="width: auto; margin-right: 5px;"><?php    echo t('Number of testimonials to show per page on /testimonials') ?></label>
						<div class="input">
							<select name="numberToDisplay">
							<?php    for($i =1; $i<=100; $i++) { ?>
								<option value="<?php    echo $i; ?>" <?php    $i == $display_number ? print 'selected="selected"' : ''; ?>><?php    echo $i; ?></option>
							<?php    } ?>
							</select>
						</div>
					</div>
					
					<input type="hidden" value="1" name="update_settings" />
					
					<?php    echo $ih->submit(t('Save Settings'), 'update_settings_form', 'left'); ?>	
					<div class="ccm-spacer">&nbsp;</div>
					
				</fieldset>			
			</form>	
				
		</div>
	</div>
</div>