$(function() {
	$('.tarotCard img').on('click', function() {
		var show = $(this).parent('a').attr('id');
		$(this).animate({
			top: '-400px'
		}, 500, 'linear', function() {
			$('.choose').fadeOut(function() {
				$('.choose img').css({
					'top': '0px'
				});
				$('#myTarotLabel').text("The Card You Drew...");
				$('.'+show).fadeIn();
			});
		});
	});

	$('#tarotreader').on('hidden', function() {
		$('.modal-body section').css({
			'display': 'none'
		});
		$('#myTarotLabel').text('Please Choose A Card?');
		$('.choose').css({
			'display': 'block'
		});
	});
})