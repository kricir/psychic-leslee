<?php
defined('C5_EXECUTE') or die("Access Denied.");
$tarot = array('aceofcu','suncard','thelove','themagi','thestar','wheel','world','x');
shuffle($tarot);
?>

<div class="tarot-reader">
	<a data-toggle="modal" data-target="#tarotreader">
		<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/tarotCards.png'; ?>" alt="Tarot Cards" />
	</a>
	<h3>Try Your Hand With Our Tarot Reader!</h3>
</div>

<div id="tarotreader" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myTarotLabel">Please Choose a Card?</h3>
	</div>
	<div class="modal-body">
		<section class="choose">
			<?php
				$i = 0;
				foreach ($tarot as $card) {
			?>
				<a href="javascript:void" id="<?php echo $card; ?>" class="tarotCard"><img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/back.gif'; ?>" style="left: <?php echo $i*40; ?>px;" /></a>
			<?php
				$i++;
				}
			?>
		</section>
		<section class="aceofcu">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/aceofcu.jpg'; ?>" alt="Ace of Cups" />
			</figure>
			<article class="definition">
				<h2>"Ace of Cups"</h2>
				<hr />
				<p>The aces are the only cards except the four of cups that the hand from above offers you a gift.  This ace offers you the beginning of great love, joy, and when the mind is filled with the spirit, fills the material to overflowing.  Great success for sure and for certain. A gift from above.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="suncard">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/suncard.jpg'; ?>" alt="Sun Card" />
			</figure>
			<article class="definition">
				<h2>"The Sun"</h2>
				<hr />
				<p>Good health, a happy marriage, happy reunions, pleasure in the simple life. She is naked because she doesn't have to hide anymore.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="thelove">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/thelove.jpg'; ?>" alt="The Love Card" />
			</figure>
			<article class="definition">
				<h2>"The Lovers"</h2>
				<hr />
				<p>The lovers stand here in a friendly harmony with nothing to hide from each other. A happy and successful life depends on the choice between worldly things and Godly things. This also says, "Do you choose the physical outer world for a relationship or the inner spiritual loving path?" The choice is between either worldy and spiritual in any situation.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="themagi">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/themagi.jpg'; ?>" alt="The Magician" />
			</figure>
			<article class="definition">
				<h2>"The Magician"</h2>
				<hr />
				<p>Pull on your higher power. You have the ability and all the tools to take the power from above and direct it into manifestation.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="thestar">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/thestar.jpg'; ?>" alt="The Star" />
			</figure>
			<article class="definition">
				<h2>"The Star"</h2>
				<hr />
				<p>Hope, insight, inspiration, unselfish aid, and the gifts of the spirit.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="wheel">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/wheel.jpg'; ?>" alt="The Wheel" />
			</figure>
			<article class="definition">
				<h2>"The Wheel"</h2>
				<hr />
				<p>This stands for perpetual motion of a fluid universe and for the flux of human life within it. New beginnings, change of fortune for the better, and unexpected turn of luck. </p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="world">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/world.jpg'; ?>" alt="The World" />
			</figure>
			<article class="definition">
				<h2>"The World"</h2>
				<hr />
				<p>Some believe it is the best card in the deck. A rival at a state of cosmic consciousness, when you have it all. Reward, success, enlightenment.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
		<section class="x">
			<figure class="card">
				<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/x.jpg'; ?>" alt="10 of Cups" />
			</figure>
			<article class="definition">
				<h2>"10 of Cups"</h2>
				<hr />
				<p>Celebration, weddings, engagements, baby announcements, lasting happiness, because it is inspired from above rather than being the sensual satisfaction.</p>
				<figure class="psychicImage">
					<img src="<?php echo DIR_REL.'/blocks/tarot_reader/img/leslee_Pic.jpg'; ?>" alt="Psychic Leslee" />
					<label class="callNow">
						<h4>Call Now For A Complete Reading<br />1-800-541-6999</h4>
					</label>
				</figure>
			</article>
		</section>
	</div>
	<div class="modal-footer">
		<button class="btn btn-success footer-close" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>

