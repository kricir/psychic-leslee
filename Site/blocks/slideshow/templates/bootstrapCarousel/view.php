<?php
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('file_set');
Loader::model('file_list');
$fs = Fileset::getByID($fileset);
$fileList = new FileList();
$fileList->filterBySet($fs);
$files = $fileList->get();
?>

<div class="cmm-carouseldisplay am-container carousel-<?php echo $bID; ?>" id="am-container">
	<?php
		foreach ($files as $f) {
	?>
				<a href="<?php echo $f->getRelativePath(); ?>" title="<?php echo $f->getDescription(); ?>" class="magnifico"><img src="<?php echo $f->getRelativePath(); ?>" /></a>
    <?php
	    }
	?>
</div>
