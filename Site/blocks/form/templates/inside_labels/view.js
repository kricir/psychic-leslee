function onEnterField() {
   if ($(this).attr('origLabel') == null)
      $(this).attr('origLabel', $(this).val());
   if ($(this).attr('origLabel') == null || $(this).attr('origLabel') == this.value)
      $(this).val("");
            
		
	$(this).prev(".formError").slideUp();
}
function onLeaveField() {	
   if ($(this).val() == '' && $(this).attr('origLabel') != null)
      $(this).val($(this).attr('origLabel'));
}

$(document).ready(function() {
	
   $('.miniSurveyView input[type=text], .miniSurveyView input[type=tel], .miniSurveyView input[type=email], textarea').click(onEnterField);
	$('.miniSurveyView input[type=text], .miniSurveyView input[type=tel], .miniSurveyView input[type=email], textarea').focus(onEnterField);
   $('.miniSurveyView input[type=text], .miniSurveyView input[type=tel], .miniSurveyView input[type=email], textarea').blur(onLeaveField);
	
	
	$('.miniSurveyView').submit(function() {
		var formErrors = 0;
		
		$('.formError').remove();
		$('input[type=text], .miniSurveyView input[type=tel], .miniSurveyView input[type=email], textarea', this).each(function() {
		    
			if (($(this).attr('origLabel') == undefined || $(this).val() == $(this).attr('origLabel')) && $(this).parent().hasClass("required")) {
				formErrors++;
				$(this).before('<div class="formError" id="formError'+formErrors+'" style="height:18px;color:red;font-size:10px;display:none;">Bitte Wert eingeben:</div>')
				$("#formError" + formErrors).slideDown(200,function() {					
				});
			}
		})

		if (formErrors > 0)
			return false;
		else
			return true;
	})
	
});
