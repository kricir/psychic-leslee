                                              <?php    
defined('C5_EXECUTE') or die(_("Access Denied."));
$survey=$controller; 
$miniSurvey=new MiniSurvey($b);
$miniSurvey->frontEndMode=true;
?>
<div class="ccm-form" id="ccm-form-id-<?php echo intval($bID)?>">
	
<?php  if ($invalidIP) { ?>
<div class="ccm-error"><p><?php echo $invalidIP?></p></div>
<?php  } ?>
<form enctype="multipart/form-data" id="miniSurveyView<?php echo intval($bID)?>" class="miniSurveyView" method="post" action="<?php  echo $this->action('submit_form')?>">
	<?php   if( $_GET['surveySuccess'] && $_GET['qsid']==intval($survey->questionSetId) ){ ?>
		<div id="msg"><?php  echo $survey->thankyouMsg ?></div> 
	<?php   }elseif(strlen($formResponse)){ ?>
		<div id="msg">
			<?php  echo $formResponse ?>
			<?php  
			if(is_array($errors) && count($errors)) foreach($errors as $error){
				echo '<div class="error">'.$error.'</div>';
			} ?>
		</div>
	<?php  } ?>
	<input name="qsID" type="hidden" value="<?php echo  intval($survey->questionSetId)?>" />
	<input name="pURI" type="hidden" value="<?php echo  $pURI ?>" />
	<?php   	
	$questionsRS = $miniSurvey->loadQuestions($survey->questionSetId, intval($bID), 0);
	$surveyBlockInfo = $miniSurvey->getMiniSurveyBlockInfoByQuestionId($survey->questionSetId,intval($bID));
	
	$showEdit = false;
	$hideQIDs=array();
	
	while($questionRow=$questionsRS->fetchRow()) {	
		if(in_array($questionRow['qID'], $hideQIDs)) continue;
		
		$msqID=intval($questionRow['msqID']);
		
		$requiredClass=($questionRow['required'])?' required ':'';
		$requiredSymbol=($questionRow['required'])?' *':'';

		echo '<div class="ccm-form-element'.$requiredClass.'">';
		$_REQUEST['Question'.$msqID] = $questionRow['question'] . $requiredSymbol;				
		
		echo $miniSurvey->loadInputType($questionRow,showEdit);
		echo "</div>";	
	}	
	
	if($surveyBlockInfo['displayCaptcha']) {		
		$captcha = Loader::helper('validation/captcha');				
		echo "<div class=\"ccm-form-captcha\">";
		
		echo '<div class="required">';		
		echo '<input type="text" value="Sicherheitscode hier eingeben" name="ccmCaptchaCode" class="ccm-input-captcha"/>';
		// Please note that we need to style our captcha, we therefore create it manually!
		//$captcha->showInput();
		echo "</div>";
		
		echo "<div>";
		$captcha->display();
		echo "</div>";
				
		echo "</div>";
		echo '<div style="clear:both"></div>';
	}
		
	echo '<input class="formBlockSubmitButton button" name="Submit" type="submit" value="Submit" />';
	
	
	?> 
</form>

</div>