<?php
defined('C5_EXECUTE') or die("Access Denied.");
class HorizontalRuleBlockController extends BlockController {
	// protected $btTable = "btHorizontalrule";
	protected $btInterfaceWidth = "550";
	protected $btInterfaceHeight = "200";

	public function getBlockTypeName() {
		return t('Horizontal Rule');
	}
	public function getBlockTypeDescription() {
		return t('Insert a dotted line anywhere on your page');
	}
    // public function save($args) {
    //     var_dump($args);
    // }
}