<?php
defined('C5_EXECUTE') or die("Access Denied.");
$fm = Loader::helper('form');
$ps = Loader::helper('form/page_selector');
?>
<div class="ccm-ui">
    <div class="setup-form">
        <div class="ccm-block-field-group">
            <?php
                echo '<h4>Title</h4>';
                echo 'Please Input a Link Title: ';
                print $fm->text('title',$title,array('placeholder'=>'Link title'));
            ?>
        </div>
        <div class="ccm-block-field-group">
            <?php
                echo '<h4>Subtitle</h4>';
                echo 'Please Input a Sub Title: ';
                print $fm->text('subtitle',$subtitle,array('placeholder'=>'Sub title'));
            ?>
        </div>
        <div class="ccm-block-field-group">
            <?php
                echo 'Please Check to Include Prayer Button: ';
                print $fm->checkbox('icon',1,$icon);
            ?>
        </div>
        <div class="ccm-block-field-group">
            <?php
                echo '<h4>Please select a page</h4>';
                print $ps->selectPage('pageID', $pageID);
            ?>
        </div>
    </div>
</div>
