<?php
defined('C5_EXECUTE') or die("Access Denied.");
$pl = Loader::helper('navigation');
?>

<?php
	$pageCID = Page::getByID($pageID);
	// var_dump($pageCID);
	$pageLink = $pl->getLinkToCollection($pageCID);
	// var_dump($pageLink);
	// echo '<a href='.$pageLink.'>'.$title.'</a>';
    // var_dump($locale);
?>
<a href="<?php echo $pageLink; ?>" class="<?php if ($icon == 1) echo 'btn-prayer' ?> pageButton">
	<span class="buttonTitle"><?php echo $title; ?></span>
	<span class="buttonSubtitle"><?php echo $subtitle ?></span>
</a>
