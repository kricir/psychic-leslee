<?php
defined('C5_EXECUTE') or die("Access Denied.");
class PageButtonBlockController extends BlockController {
	protected $btTable = "btPagebutton";
	protected $btInterfaceWidth = "550";
	protected $btInterfaceHeight = "200";

	public function getBlockTypeName() {
		return t('Page Button');
	}
	public function getBlockTypeDescription() {
		return t('Allows you to create a button that will link to another page on your site');
	}
    // public function save($args) {
    //     var_dump($args);
    // }
}