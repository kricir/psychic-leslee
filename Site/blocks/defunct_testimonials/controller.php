<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

class DefunctTestimonialsBlockController extends BlockController {
	
	var $pobj;
	
	protected $btInterfaceWidth = "400";
	protected $btInterfaceHeight = "150";
		
	
	/** 
	 * Used for localization. If we want to localize the name/description we have to include this
	 */
	public function getBlockTypeDescription() {
		return t("Displays a random testimonial");
	}
	
	public function getBlockTypeName() {
		return t("Testimonials");
	}		
	
	
	public function __construct($obj = null) {		
		parent::__construct($obj); 
	}
	
	public function view() {
		if($this->getRandom()) {
			$t = $this->getRandom();		
			$this->set('t', $t);
		} 				
	}
	
	/*
	 * required to prevent errors
	 */ 
	public function save() {
	
	}

	/*
	 * returns a random testimonial
	 */
	private function getRandom() {
		$db = Loader::db();		
		$r = $db->query('SELECT author, testimonial, optionalContent FROM pkTestimonials ORDER BY RAND() LIMIT 1');
		
		if($r && $r->numRows() > 0)
		{
			foreach($r as $row) {
				$row[] = $r;
			}		
			return $row;
		} else {
			return FALSE;
		}								
	}
}

?>