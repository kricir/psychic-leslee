$(function() {
	var spacer = ['Hmmm... Yes I see...','Okay, now it\'s becoming clear...','Yes... Yes I do believe this we are getting close','I\'m starting to see something now...','Right... Thats it keep thinking about the number']
	var message = [];	
	next = 0;
	
	$('#number').on('show', function() {
		x=0;
		var today=new Date();
		sec=0;
		for (i=1;i<=5;i++) {
			sec=today.getSeconds();
			rand1 = 31;
			while ( rand1 > 30 ) {
				rand1 = Math.random()*sec*100;
				rand1 = Math.ceil(rand1);
			}
			rand2 = Math.random();
			if (rand2>0.5) { 
				mes="add "; mes1=" to "; y=rand1;
			} else { 
				mes="subtract "; mes1=" from "; y=-rand1;
			}
			x1=x;
			x=x+y;
			if (x<0) { 
				mes="add "; mes1=" to "; x=x1+rand1;
			} 
			message.push("Please "+mes+rand1+mes1+"it. ");
			// console.log(message[1]);

		}
		message.push("Please subtract the number that you first thought of.");
		message.push("Now you have "+x+" , don`t you? Now...isn't that impressive?");
		$('.footer-close').css({'display':'none'});
		$('.next').fadeIn();
		$('.psychic-test span').text('Please think of a number?');
	}).on('hide',function() {
		message = [];
		next = 0;
	});
	
	$('.next').on('click',function(){
		if (!$(this).hasClass('disabled')) {
			$(this).addClass('disabled');
			var psychicText = $('.psychic-test span');
			psychicText.text(spacer[Math.floor(Math.random()*spacer.length)]).delay(1000).fadeOut(function() {
				psychicText.text(message[next]).fadeIn();
				next++;
				// alert(next);
				if (next == 7) {
					$('.next').fadeOut(function() {
						$('.footer-close').fadeIn();
					});
				}
				$('.next').removeClass('disabled');
			});
		}
	});

})