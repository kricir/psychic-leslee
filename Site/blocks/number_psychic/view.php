<?php
defined('C5_EXECUTE') or die("Access Denied.");
?>

<div class="number_psychic">
	<a data-toggle="modal" data-target="#number">
		<img src="<?php echo DIR_REL.'/blocks/number_psychic/img/crystalBall.png'; ?>" alt="Crystal Ball" />
	</a>
	<h3>Can Our Crystal Ball Guess Your Number?</h3>
</div>

<div id="number" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myPsychicLabel">Can Our Crystal Ball Guess Your Number?</h3>
	</div>
	<div class="modal-body">
		<img src="<?php echo DIR_REL.'/blocks/number_psychic/img/crystalBall.png'; ?>" />
		<p class="psychic-test"><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span></p>
	</div>
	<div class="modal-footer">
		<button class="btn next">Next</button>
		<button class="btn btn-success footer-close" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>

