<?php
defined('C5_EXECUTE') or die("Access Denied.");
$rssUrl = $showRss ? $controller->getRssUrl($b) : '';
$th = Loader::helper('text');
$ih = Loader::helper('image'); //<--uncomment this line if displaying image attributes (see below)
//$dh = Loader::helper('date'); //<--uncomment this line if displaying dates (see below)
//Note that $nh (navigation helper) is already loaded for us by the controller (for legacy reasons)
?>

<div class="ccm-page-list">
    <?php  foreach (array_chunk($pages,3) as $page): ?>
        <div class="row">
        <?php foreach ($page as $p):
        // Prepare data for each page being listed...
        $title = $th->entities($p->getCollectionName());
        $url = $nh->getLinkToCollection($p);
        $target = ($p->getCollectionPointerExternalLink() != '' &&
            $p->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $p->getAttribute('nav_target');
        $target = empty($target) ? '_self' : $target;
        $description = $p->getCollectionDescription();
        $description = $controller->truncateSummaries ? $th->shorten($description, $controller->truncateChars) : $description;
        $description = $th->entities($description);

        //Other useful page data...

        //$date = $dh->date(DATE_APP_GENERIC_MDY_FULL, strtotime($page->getCollectionDatePublic()));
        //If you use the date also uncomment the "$dh = Loader::helper('date');" line on top

        //$last_edited_by = $page->getVersionObject()->getVersionAuthorUserName();

        //$original_author = Page::getByID($page->getCollectionID(), 1)->getVersionObject()->getVersionAuthorUserName();

        /* CUSTOM ATTRIBUTE EXAMPLES:
         * $example_value = $page->getAttribute('example_attribute_handle');
         *
         * HOW TO USE IMAGE ATTRIBUTES:
         * 1) Uncomment the "$ih = Loader::helper('image');" line up top.
         * 2) Put in some code here like the following 2 lines:
         *      $img = $page->getAttribute('example_image_attribute_handle');
         *      $thumb = $ih->getThumbnail($img, 64, 9999, false);
         *    (Replace "64" with max width, "9999" with max height. The "9999" effectively means "no maximum size" for that particular dimension.)
         *    (Change the last argument from false to true if you want thumbnails cropped.)
         * 3) Output the image tag below like this:
         *		<img src="<?php  echo $thumb->src ?>" width="<?php  echo $thumb->width ?>" height="<?php  echo $thumb->height ?>" alt="" />
         *
         * ~OR~ IF YOU DO NOT WANT IMAGES TO BE RESIZED:
         * 1) Put in some code here like the following 2 lines:
         * 	    $img_src = $img->getRelativePath();
         * 	    list($img_width, $img_height) = getimagesize($img->getPath());
         * 2) Output the image tag below like this:
         * 	    <img src="<?php  echo $img_src ?>" width="<?php  echo $img_width ?>" height="<?php  echo $img_height ?>" alt="" />
         */

        /* End data preparation. */

        /* The HTML from here through "endforeach" is repeated for every item in the list... */ ?>
        <?php
            $img = $p->getAttribute('readerImg');
            $readerImage = $ih->getThumbnail($img,220,220,true);
        ?>
        <article class="span3 psychics">
            <a href="<?php  echo $url ?>" target="<?php  echo $target ?>" class="readers">
                <img src="<?php  echo $readerImage->src ?>" width="<?php  echo $readerImage->width ?>"
                     height="<?php echo $readerImage->height ?>" alt="<?php echo $title ?>"
                    />
            </a>
            <a href="<?php  echo $url ?>" target="<?php  echo $target ?>" class="readers">
                <h3><?php echo $p->getAttribute('readerName'); ?></h3>
            </a>
        </article>
        <?php endforeach; ?>
        </div>
    <?php  endforeach; ?>

</div><!-- end .ccm-page-list -->
