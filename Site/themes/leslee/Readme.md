# Concrete 5 New Site Starter

This package is created to bundle the Twitter Bootstrap (Sass) and initial files recommended for a packaged theme.

## Packaged Files

### Config.rb

Compiler instruction sheet for Compass

### Description.txt

Theme Name and Description declaration file

### Thumbnail.png

Theme Image Thumbnail

## Directories

### Elements

Directory containing addon theme files (Header.php, Footer.php, Sidebar.php)

### Images

Directory containing theme image files

### Javascripts

Directory containing Twitter Bootstrap individual plugin scripts, Twitter Bootstrap combined plugin script, Html5shiv for older browser support

### Sass

Directory containing Twitter Bootstrap and Twitter Bootstrap Responsive and Styles.scss. Include your themes styles in Styles.scss

### Sass/Bootstrap

Directory containing Twitter Bootstrap's individualized framework modules (For Advanced Purposes)

### Stylesheets

Directory containing outputted Scss files from Compass

## Core Theme Files

### Default.php

Every theme requires a Default.php, also doubles as a Right Sidebar Layout. Used for pages that don't have a chosen layout.

### View.php

Theme layout used to input single page layouts.

### Left_sidebar.php

Theme Page Layout for left sidebar page types.

### Full.php

Theme Page Layout for no sidebar page types.

### Typography.css

Css file for editor styles and theme customization declarations