<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
    <section class="trunk home container">
        <div class="row">
            <section class="callOuts span12">
                <div class="row">
                    <section class="callOut span3 col-xs-3">
                        <?php
                            $callout1 = new Area('Call Out 1');
                            $callout1->display($c);
                        ?>
                    </section>
                    <section class="callOut span3 col-xs-3">
                        <?php
                            $callout2 = new Area('Call Out 2');
                            $callout2->display($c);
                        ?>
                    </section>
                    <section class="callOut span3 col-xs-3">
                        <?php
                            $callout3 = new Area('Call Out 3');
                            $callout3->display($c);
                        ?>
                    </section>
                    <section class="callOut span3 col-xs-3">
                        <?php
                            $callout4 = new Area('Call Out 4');
                            $callout4->display($c);
                        ?>
                    </section>
                </div>
            </section>
            <section class="mainContent span6 push3 col-xs-6 col-xs-push-3">
                <?php
                    $main = new Area('Main');
                    $main->display($c);
                ?>
            </section>
            <aside class="sidebar span3 pull6 col-xs-3 col-xs-pull-6">
                <?php
                    $this->inc('elements/leftsidebar.php');
                ?>
            </aside>
            <aside class="sidebar span3 col-xs-3">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $this->inc('elements/footer.php');
?>