<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
    <section class="trunk container">
        <div class="row">
            <section class="mainContent span12 col-xs-12">
                <?php
                    $main = new Area('Main');
                    $main->display($c);
                ?>
            </section>
<?php
    $this->inc('elements/footer.php');
?>