<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
    <section class="trunk container">
        <div class="row">
            <section class="mainContent span9 push3 col-xs-9 col-xs-push-3">
                <?php
                    $a = new Area('Main');
                    $a->display($c);
                    print $innerContent;
                    $a = new Area('Main Extra');
                    $a->display($c);
                ?>
            </section>
            <aside class="sidebar span3 pull9 col-xs-3 col-xs-pull-9">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $this->inc('elements/footer.php');
?>