<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <section class="menu span3 col-xs-3">
                    <?php
                        $footerMain = new GlobalArea('Footer Main Menu');
                        $footerMain->disableControls();
                        $footerMain->display();
                    ?>
                </section>
                <section class="menu span3 col-xs-3">
                    <?php
                        $footerServices = new GlobalArea('Footer Services Menu');
                        $footerServices->disableControls();
                        $footerServices->display();
                    ?>
                </section>
                <section class="menu span3 col-xs-3">
                    <?php
                        $footerPsychic = new GlobalArea('Footer Psychic Menu');
                        $footerPsychic->disableControls();
                        $footerPsychic->display();
                    ?>
                </section>
                <section class="contact span3 col-xs-3">
                    <?php
                        $footerContact = new GlobalArea('Footer Contact');
                        $footerContact->disableControls();
                        $footerContact->display();
                    ?>
                </section>
            </div>
        </div>
    </footer>
    <section class="copy">
        <div class="container">
            <div class="row">
                <aside class="copyright span12 col-xs-12">
                    <p>Copyright &copy;<?php echo date('Y'); ?> <a href="http://www.scope10.com" title="Scope 10 - Mobile App Development & Web Services" target="_blank">Scope 10</a> - <a href="http://www.scope10.com" title="Scope 10 - Mobile App Development & Web Services" target="_blank">Mobile App Development</a> & <a href="http://www.scope10.com" title="Scope 10 - Mobile App Development & Web Services" target="_blank">Web Services</a> and <a href="http://www.kricir.com" title="Kricir Media Company - Minneapolis/St. Paul Web Design and Development" target="_blank">Kricir Media Company</a> - <a href="http://www.kricir.com" title="Kricir Media Company - Minneapolis/St. Paul Web Design and Development" target="_blank">Web Design and Development</a></p>
                </aside>
            </div>
        </div>
    </section>
    <div id="mobileNav">
        <?php
            $mobileNav = new GlobalArea('Mobile Nav');
            $mobileNav->disableControls();
            $mobileNav->display();
        ?>
    </div>
    </div>
    <!-- or Load Individual Bootstrap Plugins -->

    <script src="<?php echo $this->getThemePath(); ?>/javascripts/affix.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/alert.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/button.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/carousel.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/collapse.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/dropdown.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/modal.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/scrollspy.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/tab.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/tooltip.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/popover.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/javascripts/transition.js"></script>

    <!-- Additional Scripts -->
<?php Loader::element('footer_required'); ?>
</body>
</html>