<?php  defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo LANGUAGE?>" lang="<?php echo LANGUAGE?>">
<head>
	<?php Loader::element('header_required'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />

	<!-- Styles -->

	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath(); ?>/stylesheets/bootstrap.css" />
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath(); ?>/stylesheets/bootstrap-responsive.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getThemePath(); ?>/stylesheets/styles.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $this->getStyleSheet('typography.css')?>" />

	<!-- Scripts -->

	<script type="text/javascript" src="<?php echo $this->getThemePath(); ?>/javascripts/html5shiv.js"></script>
    <script type="text/javascript" src="<?php echo $this->getThemePath(); ?>/javascripts/mmenu/jquery.mmenu.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('#mobileNav').mmenu({

            }, {
                selectedClass: "nav-selected"
            });
        })
    </script>

</head>
<body>
    <div>
    <header>
        <div class="container">
            <div class="row">
                <a class="mobileNavButton visible-phone" href="#mobileNav">
                        Menu
                </a>
                <section class="logo">
                    <?php
                        $logo = new GlobalArea('Logo');
                        $logo->disableControls();
                        $logo->display();
                    ?>
                </section>
                <nav class="leftNav mainNav span4 hidden-phone">
                    <?php
                        $leftNav = new GlobalArea('Left Main Nav');
                        $leftNav->disableControls();
                        $leftNav->display();
                    ?>
                </nav>
                <nav class="rightNav mainNav span4 offset4 hidden-phone">
                    <?php
                        $rightNav = new GlobalArea('Right Main Nav');
                        $rightNav->disableControls();
                        $rightNav->display();
                    ?>
                </nav>
            </div>
        </div>
    </header>
    <section class="feature">
        <div class="container">
            <div class="row">
                <section class="telephone span4 hidden-phone">
                    <?php
                        $telephone = new GlobalArea('Telephone');
                        $telephone->disableControls();
                        $telephone->display();
                    ?>
                </section>
                <section class="social span4 offset4 hidden-phone">
                    <?php
                        $social = new GlobalArea('Social Media Icons');
                        $social->disableControls();
                        $social->display();
                    ?>
                </section>
                <?php
                    if ($c->getAttribute('FeatureArea')) {
                ?>
                <section class="slideContainer span12 hidden-phone">
                    <?php
                        $slide = new Area('Feature Area');
                        $slide->display($c);
                    ?>
                </section>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>