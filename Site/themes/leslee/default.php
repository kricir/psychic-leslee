<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
?>
    <section class="trunk container">
        <div class="row">
            <section class="mainContent span9 col-xs-9">
                <?php
                    $main = new Area('Main');
                    $main->display($c);
                ?>
            </section>
            <aside class="sidebar span3 col-xs-3">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $this->inc('elements/footer.php');
?>