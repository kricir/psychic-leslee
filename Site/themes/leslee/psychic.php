<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));
    $this->inc('elements/header.php');
    $ih = Loader::helper('image');

    $readerName = $c->getAttribute('readerName');
    $years = $c->getAttribute('clairvoyant');
    $video_script = $c->getAttribute('video_script');


?>
    <section class="trunk container">
        <div class="row">
            <section class="mainContent span9 push3 col-xs-9 col-xs-push-3">
                <div class="row">
                    <section class="readerImage span4">
                        <?php
                            $img = $c->getAttribute('readerImg');
                            $readerImage = $ih->getThumbnail($img,280,280,true);
                        ?>
                        <img src="<?php  echo $readerImage->src ?>" width="<?php  echo $readerImage->width ?>"
                             height="<?php
                        echo $readerImage->height ?>" alt="" />
                    </section>
                    <header class="readerMeta span5">
                        <h1><?php echo $readerName; ?></h1>
                        <?php if ($years) { ?>
                            <p>Years Clairvoyant: <?php echo $years; ?></p>
                        <?php } ?>
                        <?php if ($video_script) { ?>
                        <button class="btn btn-video" data-toggle="modal" data-target="#video">Video Testimonial</button>
                        <div id="video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel"><?php echo $readerName; ?></h3>
                            </div>
                            <div class="modal-body">
                                <?php echo $video_script; ?>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success footer-close" data-dismiss="modal" aria-hidden="true">Close</button>
                            </div>
                        </div>
                        <?php } ?>
                    </header>
                </div>
                <div class="row">
                        <?php
                            $main = new Area('Main');
                            $main->display($c);
                        ?>
                </div>
            </section>
            <aside class="sidebar span3 pull9 col-xs-3 col-xs-pull-9">
                <?php
                    $this->inc('elements/sidebar.php');
                ?>
            </aside>
<?php
    $this->inc('elements/footer.php');
?>